var playerName = "Alice"
var age = 21
var temperature = 72.6
var activeMember = true

age = age + 10
print(age)

if age > 30 {
    print("ok")
}

// Understanding variable
//var bonusScore: Int
//var enviromentName: String
//var levelCompleted: Bool
//var progressPercentage: Double

//var - variable; let - constant

let a = 5
let b = 2

let myResult = Double(a) / Double(b)
type(of: myResult)

//optional value - setting default value to nil
var middleName: String?

if middleName == nil {
    print("nil!")
}


var optionalInt: Int?

optionalInt = 100

if optionalInt != nil {
    //forced unwrapping
    var unwrappedInt = optionalInt!
    unwrappedInt += 5
    print(unwrappedInt)
}

if let unwrappedInt = optionalInt {
    print(unwrappedInt)
}

//Collections - array; Dictionary; Set

//Array
var letters = ["a", "b", "c"]
let sampleLetter = letters[0]

letters.append("d")
letters.removeFirst()

var sampleStringArray: [String] = []
print(sampleStringArray.count)

var abbrev = "MBf"

switch abbrev {
case "kB":
    print("kilobyte")
case "MB":
    print("megabyte")
case "du", "pa":
    print("multiple")
default:
    print("Not recognized💩")
}

// ... range operator
// there is no do-while, only repeat-while

for word in letters {
    print(word)
}


for number in 0...10 {
    print(number)
}

for number in 0..<10 {
    print(number)
}

for number in stride(from: 0, to: 256, by: 16) {
    print(number)
}

for number in stride(from: 100, through: 0, by: 5) {
    print(number)
}


// functions
func showMessage() {
    print("test function")
}

showMessage()

func showNumber(number:Int) {
    print("Number:\(number)")
}

showNumber(number:4)

func showMultipleInputs(number: Int, name: String) {
    print("Number: \(number)\tText: \(name)")
}

showMultipleInputs(number: 5, name: "test")

// return value
func basicFunction() -> String {
    let str = "Sample function"
    return str
}

let result = basicFunction()

print(result)

// returned value not used

_ = basicFunction()


// ignore variable name when calling
func showMessageWithIgnore(_ message: String) {
    print("\(message)")
}

showMessageWithIgnore("Ignore")

// argument labels
func showMessageWithArgumentLabel(txt message: String) {
    print(message)
}

showMessageWithArgumentLabel(txt: "test")


// defining own type (enumerator)

enum MediaType {
    case book
    case movie
    case music
    case game
}

var itemType: MediaType = MediaType.book
itemType = .game


switch itemType {
case .book:
    print("book")
case .movie:
    print("movie")
case .music:
    print("music")
case .game:
    print("game")
}

enum MediaTypeWithTypes {
    case movie(String)
    case music(Int)
    case book(String)
}

var firstItem: MediaTypeWithTypes = .movie("Comedy")
var secondItem: MediaTypeWithTypes = .music(1)


// Defining structs

struct Movie {
    var title: String
    var director: String
    var releaseYear: Int
    var genre: String
    
    func summary() -> String {
        return "\(title):\(director)"
    }
}

var first = Movie(title: "title", director: "director", releaseYear: 1234, genre: "genre")

print(first.title)

print(first.summary())


// Dictionaries

var airlines = ["SWA": "Southwerst Airlines",
                "BAW": "British Airways"]

// Add to dictionary
airlines["DVA"] = "Discovery Airlines"

// Remove from dict
airlines["BAW"] = nil

//var periodicElements: [String:String]

//Optional string
let resultAirlines = airlines["SWA"]
print(resultAirlines as Any)

if let resultAirlines2 = airlines["SWA"] {
    print(resultAirlines2)
} else {
    print("No match")
}

// Iterate dictionary

for (code, airline) in airlines {
    print("\(code):\(airline)")
}

// Tuple

//var basicTuple = (aperture, iso, cameraType)

func randomAlbum() -> (String, Int) {
    let title = "Title"
    let duration = 2462
    return (title, duration)
}

let resultRandomAlbum = randomAlbum()
print(resultRandomAlbum)
print(resultRandomAlbum.0)
print(resultRandomAlbum.1)


func randomAlbumWithLabels() -> (albumTitle: String, length: Int) {
    let title = "Title"
    let duration = 2462
    return (title, duration)
}


let resultRandomAlbumWithLabels = randomAlbumWithLabels()
print(resultRandomAlbumWithLabels)
print(resultRandomAlbumWithLabels.albumTitle)
print(resultRandomAlbumWithLabels.length)

let (nextTitle, length) = randomAlbum()
print(nextTitle+String(length))

// Closures (lambda)

struct Book {
    var title: String
    var pageCount: Int
}

let book1 = Book.init(title: "Book1", pageCount: 53)
let book2 = Book.init(title: "Book2", pageCount: 22)
let book3 = Book.init(title: "Book3", pageCount: 43)
let book4 = Book.init(title: "Book4", pageCount: 4)
let book5 = Book.init(title: "Book5", pageCount: 57756)

let allBooks = [book1, book2, book3, book4, book5]

func compareTwoBooks(firstBook: Book, secondBook: Book) -> Bool {
    if firstBook.pageCount <= secondBook.pageCount {
        return true
    } else {
        return false
    }
}

//let pageSortedBooks = allBooks.sorted(by: compareTwoBooks)
//print(pageSortedBooks)


//{
//    (firstBook: Book, secondBook: Book) -> Bool
//    in
//    if firstBook.pageCount <= secondBook.pageCount {
//        return true
//    } else {
//        return false
//    }
//}

let pageSortedBooks = allBooks.sorted(by: {
    (firstBook: Book, secondBook: Book) -> Bool
    in
    if firstBook.pageCount <= secondBook.pageCount {
        return true
    } else {
        return false
    }
})


// Remove redundant code

//let pageSortedBooksClean = allBooks.sorted(by: {
//    if $0.pageCount <= $1.pageCount {
//        return true
//    } else {
//        return false
//    }
//})

//let pageSortedBooksClean = allBooks.sorted(by: ) {
//    if $0.pageCount <= $1.pageCount {
//        return true
//    } else {
//        return false
//    }
//}

//let pageSortedBooksClean = allBooks.sorted {
//    if $0.pageCount <= $1.pageCount {
//        return true
//    } else {
//        return false
//    }
//}

//let pageSortedBooksClean = allBooks.sorted {
//    return $0.pageCount <= $1.pageCount
//}

let pageSortedBooksClean = allBooks.sorted { $0.pageCount <= $1.pageCount }
pageSortedBooksClean

let titleSortedBooks = allBooks.sorted { $0.title.uppercased() <= $1.title.uppercased() }
titleSortedBooks


let booksUnder10Pages = allBooks.filter { $0.pageCount < 10 }
booksUnder10Pages

//class Appliance {
//    // properties
//    var manufacturer: String = ""
//    var model: String = ""
//    var voltage: Int = 0
//    var capacity: Int?
//
//    // methods
//    func getDetails() -> String {
//        var message = "This is the \(self.model) from \(self.manufacturer)"
//        if self.voltage >= 220 {
//            message += "This model is for European usage"
//        }
//        return message
//    }
//}

// Classes

class Appliance {
    // properties
    var manufacturer: String
    var model: String
    var voltage: Int
    var capacity: Int?
    
    // initializer
    init() {
        self.manufacturer = "default manufacturer"
        self.model = "default model"
        self.voltage = 120
    }
    
    init(withVoltage: Int) {
        self.manufacturer = "default manufacturer"
        self.model = "default model"
        self.voltage = withVoltage
    }
    
    // deinitializer
    deinit {
        // perform cleanup
    }
    
    // methods
    func getDetails() -> String {
        var message = "This is the \(self.model) from \(self.manufacturer)"
        if self.voltage >= 220 {
            message += "This model is for European usage"
        }
        return message
    }
}

var kettle = Appliance()
kettle.manufacturer = "Manu"
kettle.model = "Model"
kettle.voltage = 220
print(kettle.getDetails())


// Value vs. Reference

// a Swift string is a struct- - a value type
var firstString = "This is some text"

// if I assign, it's a copy...
var secondString = firstString

secondString += " with some more text added on"
print(firstString)
print(secondString)


class Message {
    var internalText: String = "This is some text"
}


// Create new instance
var firstMessage = Message()

// if I assign, it's a reference to original instance
var secondMessage = firstMessage

secondMessage.internalText += " with some more text added on"

print(secondMessage.internalText)
print(firstMessage.internalText)

// identity operator - are they referring to the same thing?
if firstMessage === secondMessage {
    print("Same instance!")
}


// Inheritance
// superclass
class Appliance2 {
    var make: String
    var model: String
    init() {
        self.make = "default"
        self.model = "default"
    }
    
    // block overriding of function
    final func printDetails() {
        print("Make: \(self.make)\nModel: \(self.model)")
    }
}

// subclass
class Toaster: Appliance2 {
    var slices: Int
    
    override init() {
        self.slices = 2
        super.init()
    }
    
    func toast() {
        print("making toasts")
    }
}

var myToaster = Toaster()
myToaster.make = "TasterMake"
myToaster.model = "ToasterModel"
myToaster.printDetails()
myToaster.toast()


// Extensions
let album = "album name"
print(album.uppercased())

extension String {
    func removeSpaces() -> String {
        let filteredCharacters = self.filter { $0 != " " }
        return String(filteredCharacters)
    }
}

print(album.removeSpaces())

// Computed properties

class Player {
    var name: String
    var livesRemaining: Int
    var enemiesDestroyed: Int
    var penalty: Int
    var bonus: Int
    
    // computed property
    var score: Int {
        get {
            return (enemiesDestroyed * 1000) + bonus + (livesRemaining * 5000) - penalty
        }
        set {
            print("Ignoring new value \(newValue)")
            //score = newValue
        }
    }
    
    init(name: String) {
        self.name = name
        self.livesRemaining = 3
        self.enemiesDestroyed = 0
        self.penalty = 0
        self.bonus = 0
    }
}

let newPlayer = Player(name: "Ava")
newPlayer.enemiesDestroyed = 326
newPlayer.penalty = 827
newPlayer.bonus = 25000

print("Final score: \(newPlayer.score)")
newPlayer.score = 1
print("New score: \(newPlayer.score)")

print(newPlayer)


// Protocols (interfaces)

class Player2: CustomStringConvertible {
    var name: String
    var livesRemaining: Int
    var enemiesDestroyed: Int
    var penalty: Int
    var bonus: Int
    
    var description: String {
        return "Player \(self.name) has a score \(self.score)"
    }
    
    // computed property
    var score: Int {
        get {
            return (enemiesDestroyed * 1000) + bonus + (livesRemaining * 5000) - penalty
        }
        set {
            print("Ignoring new value \(newValue)")
            //score = newValue
        }
    }
    
    init(name: String) {
        self.name = name
        self.livesRemaining = 3
        self.enemiesDestroyed = 0
        self.penalty = 0
        self.bonus = 0
    }
}

let newPlayer2 = Player2(name: "Ava")
print(newPlayer2)


// Define a protocol
protocol MyProtocol {
    func showMessage()
    var name: String { get }
}

// adopt
struct MyStruct: MyProtocol {
    func showMessage() {
        print("Now conforming..")
    }
    var name: String {
        return "Sebastiano"
    }
}

// Error Handling
enum ServerError: Error {
    case noConnection
    case serverNotFound
    case authenticationRefused
}

func checkStatus(serverNumber: Int) throws -> String {
    switch serverNumber {
    case 1:
        print("You have no connection")
        throw ServerError.noConnection
    case 2:
        print("Authentication failed")
        throw ServerError.authenticationRefused
    case 3:
        print("Server 3 is up and running")
    default:
        print("Can't find that server")
        throw ServerError.serverNotFound
    }
    return "Success!"
}

let resultHandle = checkStatus(serverNumber: 1)
print(resultHandle)

do {
    let resultHandle2 = try checkStatus(serverNumber: 1)
    print(resultHandle2)
} catch ServerError.noConnection {
    print("No connection")
} catch {
    print("The problem is: \(error)")
}


// Guard - waiting for all variables
//func processTrack(trackName: String?, artist: String?, duration: Int?) {
//    guard
//}

//Defer - running functions always at the end

//defer {
//
//}
